a=0
var=$(ls | grep -E "jpg|JPG")
IFS='
'

for i in $var; do
    convert $i -resize 1024x1024! $a.bmp
    a=$( echo "$a + 1" | bc )
done
