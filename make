#!/bin/bash

if [[ $1 == "clean" ]]; then
    rm -r build
    exit
fi

mkdir build
cd build
cmake ..
make
