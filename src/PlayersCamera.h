//
// Created by Ionic on 20/6/16.
//

#ifndef COMPUTER_GRAPHICS_PROJECT_PLAYERSCAMERA_H
#define COMPUTER_GRAPHICS_PROJECT_PLAYERSCAMERA_H


#include "Camera.h"

class PlayersCamera : public Camera
{
private:
    float alphaToReach;
public:
    PlayersCamera(GameObject *rootGameObject, float height, float radius, float alpha) : Camera(
            rootGameObject, CAMERA_PERSPECTIVE, height, radius, alpha), alphaToReach(0) { }
    void SwitchCamera();
    void Update();
    void OnKeyPressed(unsigned char key, int x, int y);
};


#endif //COMPUTER_GRAPHICS_PROJECT_PLAYERSCAMERA_H
