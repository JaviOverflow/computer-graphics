#include "Camera.h"

Camera::Camera(GameObject *rootGameObject, int projectionType, float height, float radius, float alpha)
{
    childs.push_back(rootGameObject);

    this->type = projectionType;
    this->height = height;
    this->radius = radius;
    this->alpha = alpha;
}

void Camera::Reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);

    if (CAMERA_PERSPECTIVE == type)
        gluPerspective(60, (GLdouble) width / height, 1.0f, 500.0f);
    else if (CAMERA_ORTHOGONAL == type)
        glOrtho(-width / 2, width / 2, -height / 2, height / 2, 1, 500);

    glMatrixMode(GL_MODELVIEW);
}

void Camera::Render()
{
    float camera_x = cosf(alpha) * radius;
    float camera_z = sinf(alpha) * radius;

    if (CAMERA_PERSPECTIVE == type)
    {
        float inner_radius_x = cosf(alpha) * 40;
        float inner_radius_z = sinf(alpha) * 40;
        gluLookAt(camera_x, height, camera_z,
                  inner_radius_x, 0, inner_radius_z,
                  0, 1, 0);
    }
    else
    {
        gluLookAt(camera_x, height, camera_z,
                  0, 0, 0,
                  -1, 0, 0);

    }

}
