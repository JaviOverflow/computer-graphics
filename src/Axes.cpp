#include "Axes.h"

void Axes::Render()
{
    glLineWidth(3);
    glBegin(GL_LINES);
        glColor3f(1, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(1000, 0, 0);

        glColor3f(0, 1, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 1000, 0);

        glColor3f(0, 0, 1);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, 1000);
    glEnd();
}
