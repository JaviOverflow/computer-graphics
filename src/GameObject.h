#ifndef COMPUTER_GRAPHICS_PROJECT_GAMEOBJECT_H
#define COMPUTER_GRAPHICS_PROJECT_GAMEOBJECT_H

#include "General.h"
#include "Transform.h"

#include <vector>

class GameObject
{
protected:
    GLuint texture;
    std::vector<GameObject*> childs;

    virtual void Render();
    virtual void Start();
    virtual void Update();
    virtual void OnKeyPressed(unsigned char key, int x, int y);

public:
    std::string TAG;
    Transform *transform;

    GameObject();
    ~GameObject();

    void RenderAll();
    void UpdateAll();
    void OnKeyPressedAll(unsigned char key, int x, int y);
};


#endif //COMPUTER_GRAPHICS_PROJECT_GAMEOBJECT_H
