#ifndef COMPUTER_GRAPHICS_PROJECT_CAMERA_H
#define COMPUTER_GRAPHICS_PROJECT_CAMERA_H

#include "GameObject.h"

class Light : public GameObject
{
    private:
        float height;
        float radius;
        float alpha;
        void RotateLight(float speed);
        void MoveLight(float movement);
        float light_ambient;
        float light_diffuse;
        float light_specular;
        unsigned int lightid;

    public:
        Light();
        Light(unsigned int id, float radius, float alpha, float height,
            float ambient, float diffuse, float specular);
        void Render();
        void OnKeyPressed(unsigned char key, int x, int y);
};

#endif //COMPUTER_GRAPHICS_PROJECT_CAMERA_H
