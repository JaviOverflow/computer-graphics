#include "RootGameObject.h"
#include "Grid.h"
#include "Board.h"
#include "Light.h"
#include "Fog.h"

RootGameObject::RootGameObject()
{
    GameObject* board = new Board();
    childs.push_back(board);

    GameObject* light1        = new Light(GL_LIGHT0, 100, 0,    5, 0.2, 1.0, 0.5);
    GameObject* light2        = new Light(GL_LIGHT1, 100, M_PI, 5, 0.2, 1.0, 0.5);
    GameObject* centeredLight = new Light(GL_LIGHT2, 0, 0, 15, 0.2, 1.0, 0.5);
    childs.push_back(light1);
    childs.push_back(light2);
    childs.push_back(centeredLight);

    GameObject* fog = new Fog();
    childs.push_back(fog);
}

void RootGameObject::OnKeyPressed(unsigned char key, int x, int y)
{
    if (key == 27) exit(0);
}
