#include "Grid.h"

void Grid::Render()
{
    for (int i = -100; i < 100; i++)
        DrawLines(i, 10);
}

void Grid::DrawLines(int i, int scale)
{
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3d(100 * scale, 0, i * scale);
    glVertex3d(-100 * scale, 0, i * scale);

    glColor3f(0, 0, 1);
    glVertex3d(i * scale, 0, 100 * scale);
    glVertex3d(i * scale, 0, -100 * scale);
    glEnd();
}
