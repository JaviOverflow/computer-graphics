#include "HUD.h"
#include <iostream>

using namespace std;

void Font(void *font,const unsigned char *text,int x,int y)
{
	glRasterPos2i(x, y);

	while( *text != '\0' )
	{
		glutBitmapCharacter( font, *text );
		++text;
	}
}

typedef void (*ButtonCallback)();
struct Button 
{
	int   x;							/* top left x coord of the button */
	int   y;							/* top left y coord of the button */
	int   w;							/* the width of the button */
	int   h;							/* the height of the button */
	int	  state;						/* the state, 1 if pressed, 0 otherwise */
	int	  highlighted;					/* is the mouse cursor over the control? */
	unsigned char label[6];						/* the text label of the button */
	ButtonCallback callbackFunction;	/* A pointer to a function to call if the button is pressed */
};
typedef struct Button Button;

void fun2()
{
    cout << 432 << endl;
}

void HUD::Render()
{
//    glMatrixMode(GL_PROJECTION);
//    glOrtho (-1.0, 1.0f, -1.0, 1.0f, -1.0, 1.0f);
//    glMatrixMode(GL_MODELVIEW);

    glDisable(GL_DEPTH_TEST); // Disable the Depth-testing
//////////////////////////////////////////////////////////////////////////
    ButtonCallback fun = fun2;
    Button * b = new Button();
    b->x = 5;
    b->y = 5;
    b->w = 100;
    b->h = 25;
    b->state = 0;
    b->label[0]= 'P';
    b->label[1]= 'O';
    b->label[2]= 'T';
    b->label[3]= 'U';
    b->label[4]= 0;
    b->highlighted = 0;
    b->callbackFunction = fun;

	int fontx;
	int fonty;

	if(b)
	{
          glColor3f(1,0,0);
		fontx = b->x + (b->w - glutBitmapLength(GLUT_BITMAP_TIMES_ROMAN_24,b->label)) / 2 ;
		fonty = b->y + (b->h+10)/2;

		/*
		 *	if the button is pressed, make it look as though the string has been pushed
		 *	down. It's just a visual thing to help with the overall look....
		 */
		if (b->state) {
			fontx+=2;
			fonty+=2;
		}

		/*
		 *	If the cursor is currently over the button we offset the text string and draw a shadow
		 */
		if(b->highlighted)
		{
			glColor3f(1,0,0);
			Font(GLUT_BITMAP_HELVETICA_18,b->label,fontx,fonty);
			fontx--;
			fonty--;
		}

		glColor3f(1,0,0);
		Font(GLUT_BITMAP_TIMES_ROMAN_24,b->label,fontx,fonty);
	}
    //// Now draw your 2D HUD
    //// Create an orthograpic matrix for WIDTH and HEIGHT of your screen's drawing area

    glEnable(GL_DEPTH_TEST); // Enable the Depth-testing

}
