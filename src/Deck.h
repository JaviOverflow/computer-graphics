#ifndef COMPUTER_GRAPHICS_PROJECT_DECK_H
#define COMPUTER_GRAPHICS_PROJECT_DECK_H


#include "GameObject.h"
#include "Card.h"
#include <algorithm>

class Deck : public GameObject
{
private:
    std::vector<std::string> cards;

    void InitializeCardNames();

    bool hasToAnimate = false;
    int animationState = 0;
    bool axesToFlipAround[3];
public:
    Deck();

    void Render();

    void Update();

    Card *getCard();

};


#endif //COMPUTER_GRAPHICS_PROJECT_DECK_H
