#ifndef COMPUTER_GRAPHICS_PROJECT_BOARD_H
#define COMPUTER_GRAPHICS_PROJECT_BOARD_H


#include "GameObject.h"
#include "PlayerBoardSpace.h"
#include "Deck.h"

class Board : public GameObject
{
private:
    float widthX, widthZ, height;
    PlayerBoardSpace* players [2];
    Deck* deck;
    bool turn;
public:
    void OnKeyPressed(unsigned char key, int x, int y);

    Board();
    void Render();
};


#endif //COMPUTER_GRAPHICS_PROJECT_BOARD_H
