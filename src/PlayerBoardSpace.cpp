#include "PlayerBoardSpace.h"

float PlayerBoardSpace::heightCardsStack;

PlayerBoardSpace::PlayerBoardSpace()
{
    transform->position.y += 0.1;

    handCards = new Hand();
    childs.push_back(handCards);
}

void PlayerBoardSpace::AddNewCardToHand(Card *card)
{
    if (handCards->IsFull() == false && card /*!= null*/)
        handCards->AddCard(card);
}

void PlayerBoardSpace::AddCardToBoard(Card *card)
{
    PlayerBoardSpace::heightCardsStack += 0.03;

    card->transform->position = Vector3(
            (rand() % 40) - 20,
            PlayerBoardSpace::heightCardsStack,
            (rand() % 40) - 20);

    card->transform->rotation = Vector3(
            0,
            (rand() % 150) - 75,
            0);

    childs.push_back(card);
    boardCards.push_back(card);
}
