#ifndef COMPUTER_GRAPHICS_PROJECT_ROOTGAMEOBJECT_H
#define COMPUTER_GRAPHICS_PROJECT_ROOTGAMEOBJECT_H

#include "GameObject.h"

class RootGameObject : public GameObject
{
public:
    RootGameObject();
    void OnKeyPressed(unsigned char key, int x, int y);
};


#endif //COMPUTER_GRAPHICS_PROJECT_ROOTGAMEOBJECT_H
