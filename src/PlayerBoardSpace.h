//
// Created by Ionic on 19/6/16.
//

#ifndef COMPUTER_GRAPHICS_PROJECT_PLAYERBOARDSPACE_H
#define COMPUTER_GRAPHICS_PROJECT_PLAYERBOARDSPACE_H


#include "GameObject.h"
#include "Card.h"
#include "Hand.h"

class PlayerBoardSpace : public GameObject
{
private:
    std::vector<Card*> boardCards;

    static float heightCardsStack;

public:
    Hand* handCards;
    PlayerBoardSpace();

    void AddNewCardToHand(Card *card);
    void AddCardToBoard(Card *card);
};


#endif //COMPUTER_GRAPHICS_PROJECT_PLAYERBOARDSPACE_H
