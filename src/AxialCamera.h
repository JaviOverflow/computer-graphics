#ifndef COMPUTER_GRAPHICS_PROJECT_AXIALCAMERA_H
#define COMPUTER_GRAPHICS_PROJECT_AXIALCAMERA_H


#include "Camera.h"

class AxialCamera : public Camera
{
public:
    AxialCamera(GameObject *rootGameObject, float height) : Camera(
            rootGameObject, CAMERA_ORTHOGONAL, height, 0, 0) { }
};


#endif //COMPUTER_GRAPHICS_PROJECT_AXIALCAMERA_H
