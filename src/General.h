#ifndef COMPUTER_GRAPHICS_PROJECT_GENERAL_H
#define COMPUTER_GRAPHICS_PROJECT_GENERAL_H

// OpenGL includes
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


#include <cmath>
#include <string>
#include <ctime>
#include <cstdlib>
#include "BMP_Loader.h"

#define  FIXED_DELTA_TIME (1.0f/60.0f)

#endif //COMPUTER_GRAPHICS_PROJECT_GENERAL_H
