//
// Created by Ionic on 20/6/16.
//

#include "Fog.h"

void Fog::OnKeyPressed(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'F':
            glEnable(GL_FOG);
            {
                GLfloat fogColor[4] = {0, 0, 0, 1.0};
                glFogi (GL_FOG_MODE, GL_LINEAR);
                glFogfv(GL_FOG_COLOR, fogColor);
                glFogf (GL_FOG_DENSITY, 1);
                glHint (GL_FOG_HINT, GL_NICEST);
                glFogf (GL_FOG_START, 100);
                glFogf (GL_FOG_END, 200.0);
            }
            break;

        case 'f':
            glDisable(GL_FOG);
            break;

        default:
            break;
    }
}
