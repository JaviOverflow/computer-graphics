#ifndef COMPUTER_GRAPHICS_PROJECT_FOG_H
#define COMPUTER_GRAPHICS_PROJECT_FOG_H


#include "GameObject.h"

class Fog : public GameObject
{
public:
    void OnKeyPressed(unsigned char key, int x, int y);
};


#endif //COMPUTER_GRAPHICS_PROJECT_FOG_H
