//
// Created by Ionic on 18/6/16.
//

#ifndef COMPUTER_GRAPHICS_PROJECT_GRID_H
#define COMPUTER_GRAPHICS_PROJECT_GRID_H

#include "GameObject.h"

class Grid : public GameObject
{
private:
    void DrawLines(int i, int scale);

protected:
    Transform transform;

public:
    void Render();
};


#endif //COMPUTER_GRAPHICS_PROJECT_GRID_H
