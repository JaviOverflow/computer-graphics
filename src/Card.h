#ifndef COMPUTER_GRAPHICS_PROJECT_CARD_H
#define COMPUTER_GRAPHICS_PROJECT_CARD_H


#include "GameObject.h"

#define CARD_WIDTH_X 15.0f
#define CARD_WIDTH_Z 20.76f
#define CARD_HEIGHT 0.1f

class Card : public GameObject
{
public:
    GLuint backTexture;
    Card();
    Card(std::string image);
    void Render();
};


#endif //COMPUTER_GRAPHICS_PROJECT_CARD_H
