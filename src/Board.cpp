#include "Board.h"
#include "Deck.h"
#include "HUD.h"
#include "Puntuacion.h"
#include "PlayerBoardSpace.h"

void Board::Render()
{
    glColor3f(1, 1, 1);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Upwards face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 1, 0);
    glTexCoord2f(1, 0); glVertex3f(widthX/2, height/2, -widthZ/2);
    glTexCoord2f(0, 0); glVertex3f(-widthX/2, height/2, -widthZ/2);
    glTexCoord2f(0, 1); glVertex3f(-widthX/2, height/2, widthZ/2);

    glTexCoord2f(0, 1); glVertex3f(-widthX/2, height/2, widthZ/2);
    glTexCoord2f(1, 1); glVertex3f(widthX/2, height/2, widthZ/2);
    glTexCoord2f(1, 0); glVertex3f(widthX/2, height/2, -widthZ/2);
    glEnd();

    glDisable(GL_TEXTURE_2D);

    // Downwards face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, -1, 0);
    glVertex3f(-widthX/2, -height/2, widthZ/2);
    glVertex3f(-widthX/2, -height/2, -widthZ/2);
    glVertex3f(widthX/2, -height/2, -widthZ/2);

    glVertex3f(widthX/2, -height/2, -widthZ/2);
    glVertex3f(widthX/2, -height/2, widthZ/2);
    glVertex3f(-widthX/2, -height/2, widthZ/2);
    glEnd();

    // +x face
    glBegin(GL_TRIANGLES);
    glNormal3f(1, 0, 0);
    glVertex3f(widthX/2,   height/2, -widthZ/2);
    glVertex3f(widthX/2,   height/2,  widthZ/2);
    glVertex3f(widthX/2, -height/2,  -widthZ/2);

    glVertex3f(widthX/2,  height/2,  widthZ/2);
    glVertex3f(widthX/2, -height/2,  widthZ/2);
    glVertex3f(widthX/2, -height/2, -widthZ/2);
    glEnd();

    // -x face
    glBegin(GL_TRIANGLES);
    glNormal3f(-1, 0, 0);
    glVertex3f(-widthX/2,   height/2,  widthZ/2);
    glVertex3f(-widthX/2,   height/2, -widthZ/2);
    glVertex3f(-widthX/2, -height/2, -widthZ/2);

    glVertex3f(-widthX/2, -height/2,  widthZ/2);
    glVertex3f(-widthX/2,   height/2,  widthZ/2);
    glVertex3f(-widthX/2, -height/2, -widthZ/2);
    glEnd();

    // +z face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 0, 1);
    glVertex3f( widthX/2,   height/2,  widthZ/2);
    glVertex3f(-widthX/2,   height/2,  widthZ/2);
    glVertex3f(-widthX/2, -height/2,  widthZ/2);

    glVertex3f( widthX/2,   height/2,  widthZ/2);
    glVertex3f(-widthX/2, -height/2,  widthZ/2);
    glVertex3f( widthX/2, -height/2,  widthZ/2);
    glEnd();

    // -z face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 0, -1);
    glVertex3f(-widthX/2, -height/2,  -widthZ/2);
    glVertex3f(-widthX/2,   height/2,  -widthZ/2);
    glVertex3f( widthX/2,   height/2,  -widthZ/2);

    glVertex3f( widthX/2,   height/2,  -widthZ/2);
    glVertex3f( widthX/2, -height/2,  -widthZ/2);
    glVertex3f(-widthX/2, -height/2,  -widthZ/2);
    glEnd();
}

Board::Board()
{
    widthX = 200;
    widthZ = 100;
    height = 10;

    transform->position.y = -height/2;

    deck = new Deck();
    childs.push_back(deck);
    texture = Load_BMP("img/board.bmp");

    players[0] = new PlayerBoardSpace();
    GameObject* p1 = players[0];
    p1->transform->position.x += widthX/4.0f + 10;
    p1->transform->position.y += height/2;
    p1->transform->rotation.y += 90;
    childs.push_back(p1);

    players[1] = new PlayerBoardSpace();
    GameObject* p2 = players[1];
    p2->transform->position.x -= widthX/4.0f + 10;
    p2->transform->position.y += height/2;
    p2->transform->rotation.y -= 90;
    childs.push_back(p2);

    turn = true;
    for (int i = 0; i < 5; i++)
    {
        players[0]->AddNewCardToHand(deck->getCard());
        players[1]->AddNewCardToHand(deck->getCard());
    }

}

void Board::OnKeyPressed(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'c':
        case 'C':
            if (players[turn]->handCards->IsFull())
                break;

            players[turn]->AddNewCardToHand(deck->getCard());
            turn = !turn;
            break;

        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
            int position = key - '0';
            if (players[turn]->handCards->AmountCurrentCards() < position) return;
            Card* card = players[turn]->handCards->PopCard(position - 1);
            players[turn]->AddCardToBoard(card);
            if( players[turn]->handCards->AmountCurrentCards()== 1)
            {
                GameObject* hud = new HUD();
                childs.push_back(hud); // TODO arreglar chapuza
            }
            if( players[turn]->handCards->AmountCurrentCards()== 0)
            {
                GameObject* pun = new Puntuacion();
                childs.push_back(pun); // TODO arreglar chapuza
            }


            turn = !turn;
            break;
    }
}
