#include "Card.h"

void Card::Render()
{
    glColor3f(1, 1, 1);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Upwards face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 1, 0);
    glTexCoord2f(1, 1); glVertex3f(CARD_WIDTH_X/2, 0, -CARD_WIDTH_Z/2);
    glTexCoord2f(0, 1); glVertex3f(-CARD_WIDTH_X/2, 0, -CARD_WIDTH_Z/2);
    glTexCoord2f(0, 0); glVertex3f(-CARD_WIDTH_X/2, 0, CARD_WIDTH_Z/2);

    glTexCoord2f(0, 0); glVertex3f(-CARD_WIDTH_X/2, 0, CARD_WIDTH_Z/2);
    glTexCoord2f(1, 0); glVertex3f(CARD_WIDTH_X/2, 0, CARD_WIDTH_Z/2);
    glTexCoord2f(1, 1); glVertex3f(CARD_WIDTH_X/2, 0, -CARD_WIDTH_Z/2);
    glEnd();

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, backTexture);

    // Downwards face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, -1, 0);
    glTexCoord2f(1, 0); glVertex3f(-CARD_WIDTH_X/2, 0, CARD_WIDTH_Z/2);
    glTexCoord2f(1, 1); glVertex3f(-CARD_WIDTH_X/2, 0, -CARD_WIDTH_Z/2);
    glTexCoord2f(0, 1); glVertex3f(CARD_WIDTH_X/2, 0, -CARD_WIDTH_Z/2);

    glTexCoord2f(0, 1); glVertex3f(CARD_WIDTH_X/2, 0, -CARD_WIDTH_Z/2);
    glTexCoord2f(0, 0); glVertex3f(CARD_WIDTH_X/2, 0, CARD_WIDTH_Z/2);
    glTexCoord2f(1, 0); glVertex3f(-CARD_WIDTH_X/2, 0, CARD_WIDTH_Z/2);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

Card::Card()
{
    texture = Load_BMP("img/cards/10.bmp");
#warning "VARIABLE ESTATICA HA DE SER INICIALITZADA ESTATICAMEN, SINO NO GUANYAM RES BITX" 
    backTexture = Load_BMP("img/back.bmp");
}

Card::Card(std::string image)
{
    texture = Load_BMP(image.c_str());
#warning "VARIABLE ESTATICA HA DE SER INICIALITZADA ESTATICAMEN, SINO NO GUANYAM RES BITX" 
    backTexture = Load_BMP("img/back.bmp");
}
