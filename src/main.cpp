#include "General.h"
#include "RootGameObject.h"
#include "Camera.h"
#include "PlayersCamera.h"
#include "AxialCamera.h"

const int DEFAULT_WINDOW_WIDTH = 1024;
const int DEFAULT_WINDOW_HEIGHT = 720;
const char *WINDOW_TITLE = "Potu Game";

Camera* axialCamera;
Camera* playersCamera;

Camera* topLevelGameObject;

void RenderScene() { topLevelGameObject->RenderAll(); }

void ClearBuffers() { glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); }

void Display(void)
{
    ClearBuffers();
    RenderScene();
    glutSwapBuffers();
}

void Update(int timerIdentifier)
{
    glutTimerFunc(FIXED_DELTA_TIME * 1000, Update, 0);
    topLevelGameObject->UpdateAll();
    glutPostRedisplay();
}

void Keyboard(unsigned char key, int x, int y)
{
    if (key == 'p')
        topLevelGameObject =
            (topLevelGameObject == playersCamera) ? axialCamera : playersCamera;
    //if (key == 'o')
    //    glLight

    topLevelGameObject->OnKeyPressedAll(key, x, y);
    glutPostRedisplay();
}

void Reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    gluPerspective(60,(GLdouble) width/height, 1.0f, 500.0f);
    glMatrixMode(GL_MODELVIEW);
}
void WireEvents()
{
    glutReshapeFunc(Reshape);
    glutKeyboardFunc(Keyboard);
    glutTimerFunc(FIXED_DELTA_TIME * 1000, Update, 0);
    glutDisplayFunc(Display);
}

void CreateGameObjects()
{
    GameObject* rootGameObject = new RootGameObject();

    axialCamera = new AxialCamera(rootGameObject, 200);
    playersCamera = new PlayersCamera(rootGameObject, 100, 150, 0);

    topLevelGameObject = playersCamera;
}

void InitializeState()
{
    std::srand((unsigned int) std::time(0));

    glEnable(GL_DEPTH_TEST);
    glEnable (GL_COLOR_MATERIAL);
    glEnable (GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glMatrixMode(GL_TEXTURE);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    //glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_MODULATE);
    //glTexEnvi(GL_TEXTURE_ENV, GL_RGB_SCALE_ARB, 2);
    glLoadIdentity();
    glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glTranslatef(0.1f, 0, 0);
    glMatrixMode(GL_MODELVIEW);
}

void InitializeWindow()
{
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_ALPHA);

    glutCreateWindow(WINDOW_TITLE);
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    InitializeWindow();
    InitializeState();
    CreateGameObjects();
    WireEvents();

    glutMainLoop();
    return 0;
}

