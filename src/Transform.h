#ifndef COMPUTER_GRAPHICS_PROJECT_TRANSFORM_H
#define COMPUTER_GRAPHICS_PROJECT_TRANSFORM_H

#include "General.h"

struct Vector3 {
    float x, y, z;
    Vector3(float x, float y, float z) : x(x), y(y), z(z) {}
};

class Transform
{
public:
    Vector3 position = Vector3(0, 0, 0);
    Vector3 rotation = Vector3(0, 0, 0);
    Vector3 scale    = Vector3(1, 1, 1);

    void Apply();
};


#endif //COMPUTER_GRAPHICS_PROJECT_TRANSFORM_H
