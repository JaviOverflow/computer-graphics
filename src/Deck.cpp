#include "Deck.h"

#include <iostream>


#define MAX_JUMP_Y 20

#define REST_Y_POSITION 10

void Deck::Render()
{
    if (cards.size() == 0) return;

    const float DECK_HEIGHT = cards.size() * CARD_HEIGHT;

    glColor3f(1, 1, 1);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Upwards face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 1, 0);
    glTexCoord2f(1, 0);
    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glTexCoord2f(1, 1);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glTexCoord2f(0, 1);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);

    glTexCoord2f(0, 1);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glTexCoord2f(0, 0);
    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glTexCoord2f(1, 0);
    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glEnd();

    glDisable(GL_TEXTURE_2D);

    // Downwards face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, -1, 0);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);

    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glEnd();

    // +x face
    glBegin(GL_TRIANGLES);
    glNormal3f(1, 0, 0);
    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);

    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glEnd();

    // -x face
    glBegin(GL_TRIANGLES);
    glNormal3f(-1, 0, 0);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);

    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glEnd();

    // +z face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 0, 1);
    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);

    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, CARD_WIDTH_X / 2);
    glEnd();

    // -z face
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 0, -1);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);

    glVertex3f(CARD_WIDTH_Z / 2, DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glVertex3f(-CARD_WIDTH_Z / 2, -DECK_HEIGHT / 2, -CARD_WIDTH_X / 2);
    glEnd();
}

Deck::Deck()
{
    texture = Load_BMP("img/back.bmp");

    transform->position.y = REST_Y_POSITION;
    transform->position.z = -30;

    InitializeCardNames();
}

void Deck::InitializeCardNames()
{
    for (int i = 0; i < 56; i++)
    {
        char buff[100];
        snprintf(buff, sizeof(buff), "img/cards/%i.bmp", i);
        std::string buffAsStdStr = buff;
        cards.push_back(buffAsStdStr);
    }
    random_shuffle(cards.begin(), cards.end());
}

Card *Deck::getCard()
{
    if (cards.empty()) return NULL;
    Card *card = new Card(cards.back());
    cards.pop_back();
    hasToAnimate = true;

    return card;
}

void Deck::Update()
{
    switch (animationState)
    {
        case 0:
            if (hasToAnimate)
            {
                hasToAnimate = false;
                animationState = 1;
                axesToFlipAround[0] = (rand() % 2) == 0;
                axesToFlipAround[1] = (rand() % 2) == 0;
                axesToFlipAround[2] = (rand() % 2) == 0;

                if (axesToFlipAround[0] == false && axesToFlipAround[1] == false && axesToFlipAround[2] == false)
                    axesToFlipAround[0] = true;
            }
            break;

        case 1:
            transform->position.y += FIXED_DELTA_TIME * MAX_JUMP_Y * 2;

            if (axesToFlipAround[0])
                transform->rotation.x += 360 * FIXED_DELTA_TIME;
            if (axesToFlipAround[1])
                transform->rotation.y += 360 * FIXED_DELTA_TIME;
            if (axesToFlipAround[2])
                transform->rotation.z += 360 * FIXED_DELTA_TIME;

            if (transform->position.y > MAX_JUMP_Y + REST_Y_POSITION)
                animationState = 2;
            break;

        case 2:
            transform->position.y -= FIXED_DELTA_TIME * MAX_JUMP_Y * 2;

            if (axesToFlipAround[0])
                transform->rotation.x += 360 * FIXED_DELTA_TIME;
            if (axesToFlipAround[1])
                transform->rotation.y += 360 * FIXED_DELTA_TIME;
            if (axesToFlipAround[2])
                transform->rotation.z += 360 * FIXED_DELTA_TIME;

            if (transform->position.y < REST_Y_POSITION)
                animationState = 3;
            break;

        case 3:
            transform->rotation.x = 0;
            transform->rotation.y = 0;
            transform->rotation.z = 0;
            transform->position.y = REST_Y_POSITION;
            animationState = 0;
            break;

        default:
            break;
    }
}
