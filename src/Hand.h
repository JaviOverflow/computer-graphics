#ifndef COMPUTER_GRAPHICS_PROJECT_HAND_H
#define COMPUTER_GRAPHICS_PROJECT_HAND_H


#include "GameObject.h"
#include "Card.h"

class Hand : public GameObject
{
public:
    Hand();
    bool IsFull();
    unsigned long AmountCurrentCards();
    void AddCard(Card* card);
    Card* PopCard(int index);

    void ReOrganizeCardsInSpace();
};


#endif //COMPUTER_GRAPHICS_PROJECT_HAND_H
