#ifndef COMPUTER_GRAPHICS_PROJECT_CAMERA_H
#define COMPUTER_GRAPHICS_PROJECT_CAMERA_H

#include "GameObject.h"

#define CAMERA_PERSPECTIVE 0
#define CAMERA_ORTHOGONAL 1

class Camera : public GameObject
{
protected:
    int type;
    float height;
    float radius;
    float alpha;
public:
    Camera(GameObject *rootGameObject, int projectionType, float height, float radius, float alpha);
    void Reshape(int width, int height);
    void Render();
};


#endif //COMPUTER_GRAPHICS_PROJECT_CAMERA_H
