#ifndef COMPUTER_GRAPHICS_PROJECT_HUD_H
#define COMPUTER_GRAPHICS_PROJECT_HUD_H


#include "GameObject.h"

class HUD : public GameObject
{
public:
    GLuint backTexture;
    void Render();
};


#endif //COMPUTER_GRAPHICS_PROJECT_HUD_H
