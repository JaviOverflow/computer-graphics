//
// Created by Ionic on 19/6/16.
//

#ifndef COMPUTER_GRAPHICS_PROJECT_AXES_H
#define COMPUTER_GRAPHICS_PROJECT_AXES_H


#include "GameObject.h"

class Axes : public GameObject
{
public:
    void Render();
};


#endif //COMPUTER_GRAPHICS_PROJECT_AXES_H
