#include "Light.h"
#include "RootGameObject.h"

void Light::Render()
{
    float light_x = cosf(alpha) * radius;
    float light_z = sinf(alpha) * radius;

    glPushMatrix();
        glColor3f(0.5,1,0.5);
        glTranslatef(light_x, height, light_z);
        float position[] = {0, 0, 0, 1};
        glLightfv(lightid, GL_POSITION, position);
        float ambient[] = {light_ambient, light_ambient, light_ambient, 1};
        glLightfv(lightid, GL_AMBIENT, ambient);
        float diffuse[] = {light_diffuse, light_diffuse, light_diffuse, 1};
        glLightfv(lightid, GL_DIFFUSE, diffuse);
        float specular[] = {light_specular, light_specular, light_specular, 1};
        glLightfv(lightid, GL_SPECULAR, specular);
    glPopMatrix();
}

Light::Light(unsigned int id, float radius, float alpha, float height,
            float ambient, float diffuse, float specular)
{
    this->lightid = id;
    this->radius = radius;
    this->alpha = alpha;
    this->height = height;
    this->light_ambient = ambient;
    this->light_diffuse = diffuse;
    this->light_specular= specular;
}

Light::Light()
{
    lightid = GL_LIGHT0;
    radius = 100;
    alpha = 0;
    height = 10;
    light_ambient = 0.2;
    light_diffuse = 1.0;
    light_specular= 0.5;
}

void Light::RotateLight(float speed)
{
    alpha += 8/360.0*speed;
}

void Light::MoveLight(float movement)
{
    radius += movement;
}

void Light::OnKeyPressed(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'k':
            RotateLight(1);
            break;
        case 'K':
            RotateLight(-1);
            break;
        case 'e':
            MoveLight(1);
            break;
        case 'E':
            MoveLight(-1);
            break;
        case 'm':
            if (lightid == GL_LIGHT0)
                glDisable(GL_LIGHT0);
            break;
        case 'M':
            if (lightid == GL_LIGHT0)
                glEnable(GL_LIGHT0);
            break;
        case 'n':
            if (lightid == GL_LIGHT1)
                glDisable(GL_LIGHT1);
            break;
        case 'N':
            if (lightid == GL_LIGHT1)
                glEnable(GL_LIGHT1);
            break;
        case 'b':
            if (lightid == GL_LIGHT2)
                glDisable(GL_LIGHT2);
            break;
        case 'B':
            if (lightid == GL_LIGHT2)
                glEnable(GL_LIGHT2);
            break;
        default:
            break;
    }
}
