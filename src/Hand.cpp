#include "Hand.h"

#define HAND_SIZE 5
#define X_SPACE 17

Hand::Hand()
{
    transform->position.z += 30;
    transform->position.y += 5;
    transform->rotation.x += 30;
}

bool Hand::IsFull()
{
    return childs.size() >= HAND_SIZE;
}

void Hand::AddCard(Card *card)
{
    childs.push_back(card);
    ReOrganizeCardsInSpace();
}

Card* Hand::PopCard(int index)
{
    Card* card = (Card *) childs[index];
    childs.erase(childs.begin() + index);
    ReOrganizeCardsInSpace();
    return card;
}

void Hand::ReOrganizeCardsInSpace()
{
    const int x_position[5] = {0, X_SPACE, -X_SPACE, 2 * X_SPACE, -2 * X_SPACE};
    for (int i = 0; i < childs.size(); i++)
        childs[i]->transform->position.x = x_position[i];
}

unsigned long Hand::AmountCurrentCards()
{
    return childs.size();
}
