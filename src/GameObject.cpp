#include "GameObject.h"

GameObject::~GameObject()
{
    for (auto const& child: childs)
        delete child;

    delete transform;
}

void GameObject::OnKeyPressedAll(unsigned char key, int x, int y)
{
    for (auto const& child: childs)
        child->OnKeyPressedAll(key, x, y);

    OnKeyPressed(key, x, y);
}

void GameObject::OnKeyPressed(unsigned char key, int x, int y) { }

void GameObject::RenderAll()
{
    glPushMatrix();
        transform->Apply();

        Render();

        for (auto const& child: childs)
            child->RenderAll();
    glPopMatrix();
}

void GameObject::Render() { }

void GameObject::UpdateAll()
{
    for (auto const& child: childs)
        child->UpdateAll();

    Update();
}

void GameObject::Update() { }

GameObject::GameObject()
{
    transform = new Transform();

    Start();
}

void GameObject::Start() { }

