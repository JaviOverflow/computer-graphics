#include "PlayersCamera.h"

void PlayersCamera::SwitchCamera()
{
    alphaToReach += M_PI;

    if (alpha > 2 * M_PI)
    {
        alpha -= 2 * M_PI;
        alphaToReach -= 2 * M_PI;
    }
}

void PlayersCamera::Update()
{
    if (alpha < alphaToReach)
        alpha += (1.5 * M_PI_2 * FIXED_DELTA_TIME);
}

void PlayersCamera::OnKeyPressed(unsigned char key, int x, int y)
{
    if (key == 's')
        SwitchCamera();
}
